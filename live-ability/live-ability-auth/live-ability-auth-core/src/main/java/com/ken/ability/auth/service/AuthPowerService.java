package com.ken.ability.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ken.entity.auth.AuthPower;

/**
 * 权限表(AuthPower)表服务接口
 *
 * @author makejava
 * @since 2021-09-12 12:29:33
 */
public interface AuthPowerService extends IService<AuthPower> {

}
