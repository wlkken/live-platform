package com.ken.ability.auth.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ken.ability.auth.service.AuthUserService;
import com.ken.entity.auth.AuthUser;
import com.ken.entity.auth.qo.AuthUserQo;
import com.ken.mapper.auth.dao.AuthUserDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户表(AuthUser)表服务实现类
 *
 * @author makejava
 * @since 2021-09-12 12:28:03
 */
@Service
@Slf4j
public class AuthUserServiceImpl extends ServiceImpl<AuthUserDao, AuthUser> implements AuthUserService {

    @Autowired
    private AuthUserDao authUserDao;

    /**
     * 根据用户名查询用户信息 - 关联查询出角色和权限的信息
     * @param username
     * @return
     */
    @Override
    public AuthUser queryAuthUserByUserName(String username) {
        AuthUserQo qo = new AuthUserQo().setUsername(username);
        return authUserDao.queryAuthUser(qo);
    }
}
