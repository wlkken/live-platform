package com.ken.ability.auth.protocol.security;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;

@Data
@Slf4j
public class AuthRoleDetails implements GrantedAuthority {
    //主键
    private Long id;
    //角色名称
    private String roleName;
    //角色别名
    private String roleTag;

    @Override
    public String getAuthority() {
        return roleTag;
    }
}
