package com.ken.ability.gateway.application;

import com.ken.ability.gateway.loadbalance.GrayLoadBalanceRule;
import com.ken.ability.gateway.routes.NacosDynamicRouteDefinitionRepository;
import com.ken.common.sentinel.annotation.EnableSentinelCluster;
import com.ken.common.sentinel.config.SentinelClusterStatus;
import com.netflix.loadbalancer.IRule;
import org.springframework.cloud.gateway.route.RouteDefinitionRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@EnableSentinelCluster(SentinelClusterStatus.GATEWAY)
public class GatewayConfiguration {

    /**
     * 配置基于nacos的动态路由
     * @return
     */
    @Bean
    public RouteDefinitionRepository nacosRouteDefinitionRepository(){
        return new NacosDynamicRouteDefinitionRepository();
    }

    /**
     * 注册灰度发布
     * @return
     */
    @Bean
    @Scope(value = "prototype")
    public IRule getGragRule(){
        return new GrayLoadBalanceRule();
    }
}
