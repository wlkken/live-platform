package com.ken.ability.gateway.auth.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "spring.security")
@Data
@RefreshScope
public class AuthIgnoreProperties {

    /**
     * 忽略的url
     */
    private List<String> ignore;
}
