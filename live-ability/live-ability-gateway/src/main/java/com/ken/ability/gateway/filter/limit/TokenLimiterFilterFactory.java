package com.ken.ability.gateway.filter.limit;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class TokenLimiterFilterFactory extends AbstractGatewayFilterFactory<TokenLimiterFilterFactory.MyConfig> {

    private TokenLimiterFilter tokenLimiterFilter;

    public TokenLimiterFilterFactory() {
        super(MyConfig.class);
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Arrays.asList("maxTokens", "secTokens");
    }

    @Override
    public GatewayFilter apply(MyConfig config) {
        tokenLimiterFilter = new TokenLimiterFilter();
        Integer maxTokens = config.getMaxTokens();
        Integer secTokens = config.getSecTokens();
        tokenLimiterFilter.setMaxTokens(maxTokens);
        tokenLimiterFilter.setSecTokens(secTokens);
        return tokenLimiterFilter;
    }

    @Override
    public String name() {
        return "tokenlimite";
    }

    @Data
    @Accessors(chain = true)
    public static class MyConfig {
        private Integer maxTokens;
        private Integer secTokens;
    }
}
