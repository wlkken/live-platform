package com.ken.ability.search.handle;

import com.ken.ability.search.service.ISysLogService;
import com.ken.common.event.apply.handle.EventHandler;
import com.ken.common.event.apply.handle.annotation.EventType;
import com.ken.common.event.constact.EventConstact;
import com.ken.common.event.framework.message.EventMessage;
import com.ken.data.entity.syslog.SysLog;
import org.springframework.beans.factory.annotation.Autowired;

@EventType(EventConstact.SYS_LOG_MSG)
public class SysLogMsgHandler implements EventHandler<SysLog> {

    @Autowired
    private ISysLogService sysLogService;

    @Override
    public void eventHandler(SysLog sysLog, EventMessage<SysLog> eventMessage) {
        sysLogService.insertSysLog(sysLog);
    }
}
