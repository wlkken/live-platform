package com.ken.common.auth.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Slf4j
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        log.debug("[Web-Security-Config] - Security核心配置启动");
        http.httpBasic().disable()
                .formLogin().disable()
                .authorizeRequests()
                .anyRequest().permitAll()
                .and().csrf().disable();
    }
}
