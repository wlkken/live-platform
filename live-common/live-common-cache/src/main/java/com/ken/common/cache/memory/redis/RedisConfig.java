package com.ken.common.cache.memory.redis;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;

@Configuration
@ConditionalOnProperty(prefix = "kenplugin.cache", value = "clusterType", havingValue = "REDIS", matchIfMissing = true)
public class RedisConfig {

//    @Bean("cacheRedisProperties")
//    @ConditionalOnBean
//    public RedisProperties getRedisProperties(RedisProperties redisProperties){
//        RedisProperties cacheRedisProperties = new RedisProperties();
//        BeanUtils.copyProperties(redisProperties, cacheRedisProperties);
//        cacheRedisProperties.setDatabase(15);
//        return cacheRedisProperties;
//    }
//
//    @Bean("cacheRedisConnectionFactory")
//    @ConditionalOnBean
//    public RedisConnectionFactory getRedisConnectionFactory(RedisConnectionFactory redisConnectionFactory){
//        LettuceConnectionFactory lettuceConnectionFactory = new LettuceConnectionFactory();
//    }

    /**
     * 配置redis的key和value的序列化方式
     * @param redisTemplate
     * @return
     */
    @Bean
    public RedisTemplate getRedisTemplate(RedisTemplate redisTemplate){
        redisTemplate.setKeySerializer(new JdkSerializationRedisSerializer());
        redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
        return redisTemplate;
    }
}
