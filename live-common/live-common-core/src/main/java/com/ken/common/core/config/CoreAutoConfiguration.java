package com.ken.common.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.ken.ability", "com.ken.business", "com.ken.common.core"})
public class CoreAutoConfiguration {
}
