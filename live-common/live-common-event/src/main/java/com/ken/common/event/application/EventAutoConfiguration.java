package com.ken.common.event.application;

import com.ken.common.event.application.annotation.EnableEvent;
import com.ken.common.event.apply.handle.EventHandler;
import com.ken.common.event.apply.utils.EventUtils;
import com.ken.common.event.core.rabbitmq.config.RabbitMqConfig;
import com.ken.common.event.framework.generate.MsgIdGenerate;
import com.ken.common.event.framework.generate.impl.UUIDGenerate;
import com.ken.common.event.framework.handle.MsgHandler;
import com.ken.common.event.framework.handle.defaults.DefaultMsgHandler;
import com.ken.common.event.framework.message.factory.MsgFactory;
import com.ken.common.event.framework.message.factory.defaults.DefaultMsgFactory;
import com.ken.common.event.framework.processor.EventMsgPostProcessor;
import com.ken.common.event.framework.processor.idempotent.IdempotentProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;

/**
 * 事件总监主配置类
 */
@Configuration
@EnableConfigurationProperties(EventProperties.class)
public class EventAutoConfiguration {

    @Configuration
    @Order
    @ConditionalOnBean(annotation = EnableEvent.class)
    @ComponentScan("com.ken.common.event.plugins")
    public static class PluginsConfiguration{

    }

    /**
     * 生产端的Bean配置
     */
    @Configuration
    public static class ProviderConfiguration{

        /**
         * 注入事件工具类
         * @return
         */
        @Bean
        public EventUtils getEventUtils(){
            return new EventUtils();
        }

        /**
         * 消息对象的生产工厂对象
         * @return
         */
        @Bean
        @ConditionalOnMissingBean
        public MsgFactory getMsgFactory(){
            return new DefaultMsgFactory();
        }

        /**
         * 消息id生成器
         * @return
         */
        @Bean
        @ConditionalOnMissingBean
        public MsgIdGenerate getMsgIdGenerate(){
            return new UUIDGenerate();
        }
    }

    /**
     * 消费端的Bean配置
     */
    @Configuration
    @ConditionalOnBean(value = EventHandler.class)
    public static class ConsumerConfiguration{

        /**
         * 注册架构层消息处理器
         * @return
         */
        @Bean
        @ConditionalOnMissingBean
        public MsgHandler getMsgHandler(){
            return new DefaultMsgHandler();
        }

        /**
         * 注册消息接收前后置处理器
         * @return
         */
        @Bean
        public EventMsgPostProcessor getIdempotentProcessor(){
            return new IdempotentProcessor();
        }
    }


    /**
     * rabbitMq的配置类
     * @return
     */
    @Configuration
    @ConditionalOnBean(value = EventHandler.class)
    @ConditionalOnProperty(prefix = "kenplugin.event", value = "eventMq", havingValue = "Rabbit_MQ", matchIfMissing = true)
    @Import(RabbitMqConfig.class)
    public static class RabbitMqConfiguration{
    }

}
