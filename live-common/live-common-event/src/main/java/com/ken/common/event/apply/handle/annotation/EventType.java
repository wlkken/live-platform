package com.ken.common.event.apply.handle.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface EventType {

    String value();

    boolean isAsync() default false;
}
