package com.ken.common.event.framework.processor;

import com.ken.common.event.framework.message.EventMessage;

/**
 * 消息发送前的处理器
 */
public interface SendMsgPostProcessor {

    default boolean isSupport(String eventTypeStr, EventMessage eventMessage){
        return true;
    }

    default boolean beforeProcessor(String eventTypeStr, EventMessage eventMessage){
        return true;
    }
}
