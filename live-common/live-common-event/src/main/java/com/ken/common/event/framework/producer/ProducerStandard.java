package com.ken.common.event.framework.producer;

import com.ken.common.event.apply.delay.DelayTime;
import com.ken.common.event.framework.message.EventMessage;

import java.util.concurrent.TimeUnit;

/**
 * 事件总线发布者相关协议
 */
public interface ProducerStandard {

    /**
     * 发送事件消息
     * @param message
     */
    default void sendEvent(String eventType, EventMessage message){
        //默认按非持久化发送
        sendEvent(eventType, message, false);
    }

    /**
     * 发送事件消息
     * @param message 消息内容
     * @param durable 是否持久化
     */
    void sendEvent(String eventType, EventMessage message, boolean durable);


    /**
     * 发送延迟事件消息 - 部分底层实现不支持
     * @param message
     * @param durable
     * @param time
     * @param unit
     */
    default void sendDelayEvent(String eventType, EventMessage message, boolean durable, long time, TimeUnit unit){
    }

    /**
     * 发送延迟事件消息 - 根据枚举选择过期的时间
     * @param message
     * @param durable
     * @param delayTime
     */
    void sendDelayEvent(String eventType, EventMessage message, boolean durable, DelayTime delayTime);

}
