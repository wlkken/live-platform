package com.ken.common.event.plugins.msghandler.chain;

import com.ken.common.event.framework.message.EventMessage;
import lombok.Data;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * 责任链模式抽象父类
 */
public abstract class AbstractMsgChain<F> {

    private AbstractMsgChain nextMsgChain;

    //设置下一个责任对象
    public void setNext(AbstractMsgChain msgChain) {
        this.nextMsgChain = msgChain;
    }

    /**
     * 执行下一个责任对象
     */
    public void nextInvoce(ChainContext contxt, Object data){
        if (nextMsgChain != null) {
            //获得对象的泛型类型
            Type type = nextMsgChain.getClass().getGenericSuperclass();
            Type actualTypeArgument = ((ParameterizedType) type).getActualTypeArguments()[0];
            Class gType = (Class) actualTypeArgument;
            System.out.println("下一个对象的泛型类型为：" + gType.getName());

            //判断传递的参数类型和下一个责任链处理的数据类型是否匹配，如果不匹配则跳过下一个责任链
            if (gType.isAssignableFrom(data.getClass())) {
                //执行下一个责任的核心运行方法
                nextMsgChain.coreRun(contxt, data);
            } else {
                //如果类型不匹配，则直接跳过下一个责任链
                nextMsgChain.nextInvoce(contxt, data);
            }
        }
    }

    /**
     * 核心运行方法
     */
    public abstract void coreRun(ChainContext context, F data);

    /**
     * 责任链参数对象
     */
    @Data
    public static class ChainContext{
        //事件类型
        String eventTypeStr;
        //事件消息
        EventMessage eventMessage;
    }
}
