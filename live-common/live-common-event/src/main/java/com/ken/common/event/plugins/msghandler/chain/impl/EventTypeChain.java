package com.ken.common.event.plugins.msghandler.chain.impl;


import com.ken.common.event.apply.handle.EventHandler;
import com.ken.common.event.apply.handle.annotation.EventType;
import com.ken.common.event.plugins.msghandler.chain.AbstractMsgChain;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 根据事件类型找到对应事件处理器的责任对象
 */
@Slf4j
@Component
@ConditionalOnBean(EventHandler.class)
@Order(0)
public class EventTypeChain extends AbstractMsgChain {

    @Autowired
    private List<EventHandler> eventHandlers;

    @Override
    public void coreRun(ChainContext context, Object object) {

        //循环多个事件处理器
        A:for (EventHandler eventHandler : eventHandlers) {
            //获得EventHandler实现类的注解
            EventType eventType = eventHandler.getClass().getAnnotation(EventType.class);
            //判断事件类型是否匹配
            if (!context.getEventTypeStr().equals(eventType.value())) continue A;

            //透传调用下一个责任链
            super.nextInvoce(context, eventHandler);
        }
    }
}
