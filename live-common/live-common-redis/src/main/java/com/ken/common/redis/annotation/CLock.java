package com.ken.common.redis.annotation;

import com.ken.common.redis.mode.LockMode;

import java.lang.annotation.*;

/**
 * 分布式锁注解
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CLock {

    /**
     * 上锁的key
     * @return
     */
    String key();

    /**
     * 上锁的模式 - 自动/手动
     * @return
     */
    LockMode lockMode() default LockMode.AUTO;
}
