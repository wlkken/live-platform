package com.ken.common.sentinel.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import java.util.Properties;

@Data
@ConfigurationProperties(prefix = "spring.cloud.sentinel.datasource.nacos")
public class SentinelNacosProperties {

    /**
     * 服务地址
     */
    private String serverAddr;

    /**
     * 相关命名空间
     */
    private String namespace;

    /**
     * 组id
     */
    private String groupId;

    /**
     * 服务端命名空间的dataid
     */
    private String namespaceSetDataId;

    /**
     * 客户端基本配置的dataid
     */
    private String clientConfigDataId;

    /**
     * 流控规则后缀
     */
    private String flowRulePrefix;

    /**
     * 参数流控后缀
     */
    private String flowParamRulePrefix;

    /**
     * 降级熔断后缀
     */
    private String degradeRulePrefix;

    /**
     * 服务的相关配置
     */
    private Properties serverProperties;

    @PostConstruct
    public void init(){
        serverProperties = new Properties();
        serverProperties.put("serverAddr", serverAddr);
        serverProperties.put("namespace", namespace);
    }
}
