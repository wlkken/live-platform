package com.ken.common.web.config;

import com.alibaba.cloud.nacos.NacosConfigAutoConfiguration;
import com.ken.common.web.async.AsyncThreadsConfiguration;
import com.ken.common.web.async.WebAsyncConfig;
import com.ken.common.web.feign.FeignDefaultRequestInterceptor;
import com.ken.common.web.feign.FeignProperties;
import com.ken.common.web.http.ResponseHandler;
import com.ken.common.web.interception.WebAuthInterception;
import com.ken.common.web.syslog.aop.SysLogAop;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
public class BaseAutoConfiguration {

    @Configuration
    @ConditionalOnClass(FeignAutoConfiguration.class)
    @EnableConfigurationProperties(FeignProperties.class)
    @EnableFeignClients(basePackages = "com.ken", defaultConfiguration = FeignDefaultRequestInterceptor.class)
    public static class FeignConfig{
    }

    @Configuration
    @ConditionalOnBean(NacosConfigAutoConfiguration.class)
    @EnableDiscoveryClient
    public static class NacosDiscoveryConfig{
    }

    @Configuration
    @ConditionalOnWebApplication
    @ConditionalOnClass(ResponseHandler.class)
    @Import(ResponseHandler.class)
    public static class WebMvcRepsonseConfig{

        @Bean
        public SysLogAop getSysLogAop(){
            return new SysLogAop();
        }

        @Bean
        public WebInterConfiguration getWebInterConfiguration(){
            return new WebInterConfiguration();
        }

        @Bean
        public WebAuthInterception getWebAuthInterception(){
            return new WebAuthInterception();
        }
    }

    @Configuration
    @ConditionalOnBean(annotation = EnableAsync.class)
    @EnableConfigurationProperties(AsyncThreadsConfiguration.class)
    @Import(WebAsyncConfig.class)
    public static class AsyncConfig{
    }


}
