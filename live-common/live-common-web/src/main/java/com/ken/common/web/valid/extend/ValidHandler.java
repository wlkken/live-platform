package com.ken.common.web.valid.extend;

import com.ken.common.web.valid.annotation.KenValid;

/**
 * 拓展的校验器接口
 */
public interface ValidHandler<T> {

    /**
     * 实际的校验执行方法
     * @param kenValid - 当前注解对象
     * @param data - 校验的目标对象
     * @return
     */
    boolean handler(KenValid kenValid, T data);
}
