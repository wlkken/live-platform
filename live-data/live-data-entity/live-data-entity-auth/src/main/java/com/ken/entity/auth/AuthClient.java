package com.ken.entity.auth;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ken.entity.base.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 认证客户端表(AuthClient)表实体类
 *
 * @author makejava
 * @since 2021-09-12 11:27:23
 */
@Data
@Accessors(chain = true)
public class AuthClient extends BaseEntity {
    //客户端id
    @TableId(type = IdType.AUTO)
    private Long id;
    //客户端名称
    private String clientId;
    //客户端密钥(加密)
    private String clientSecret;
    //授权Token的有效期
    private Integer accessTokenTime;
    //刷新Token的有效期
    private Integer refreshTokenTime;
    //设置该客户端的授权模式
    private String authGrantType;
    //重定向的url
    private String redirectUrl;
    //适用范围
    private String scope;
}
