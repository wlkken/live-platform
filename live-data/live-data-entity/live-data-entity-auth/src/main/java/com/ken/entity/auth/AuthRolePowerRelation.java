package com.ken.entity.auth;

import com.ken.entity.base.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 角色权限关联表(AuthRolePowerRelation)表实体类
 *
 * @author makejava
 * @since 2021-09-12 11:27:24
 */
@Data
@Accessors(chain = true)
public class AuthRolePowerRelation extends BaseEntity {
    //角色id auth_role表id
    private Long rid;
    //权限id auth_power表id
    private Long pid;
}
