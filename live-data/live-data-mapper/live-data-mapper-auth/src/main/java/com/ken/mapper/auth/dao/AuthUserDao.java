package com.ken.mapper.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ken.entity.auth.AuthUser;
import com.ken.entity.auth.qo.AuthUserQo;
import com.ken.mybatis.annotation.AutoMapping;

/**
 * 用户表(AuthUser)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-12 12:03:36
 */
public interface AuthUserDao extends BaseMapper<AuthUser> {

    @AutoMapping
    AuthUser queryAuthUser(AuthUserQo authUserQo);
}
